import webdriver from 'selenium-webdriver';
import AccessPage from "../pages/access-page.js";

export default class AccessActions {
    constructor(driver) {
        var accessPage = new AccessPage(driver);
        this.accessPage = accessPage;
    }

    waitForPageToLoad(){
        this.accessPage.waitToLoad();
    }

    openRoleModalForm(){
        this.accessPage.clickRoleQA1Item();
        this.accessPage.waitForRolePopupToAppear();
        this.accessPage.clickNodesTab();
    }

    //TODO: add check for fetched nodes
    addAllNodesInProcessToRole(processName){
        this.accessPage.clickProcessItem(processName);
        this.accessPage.clickAddAllNodesButton(processName);
        this.accessPage.clickDoneButton();        
    }
};

