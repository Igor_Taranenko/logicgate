import webdriver from 'selenium-webdriver';
import config from '../../config/settings.json';
import LoginPage from "../pages/login-page.js";

export default class LoginActions {
    constructor(driver) {
        var loginPage = new LoginPage(driver);
        this.loginPage = loginPage;
    }

    waitForPageToLoad(){
        this.loginPage.waitToLoad();
    }

    /**
     * Fills and submits login form
     * @param {string} username 
     * @param {string} password 
     */
    login(username, password){
        this.loginPage.setUsername(username);
        this.loginPage.setPassword(password);
        this.loginPage.clickLoginButton();
    }
};
