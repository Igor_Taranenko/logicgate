import webdriver from 'selenium-webdriver';
import MainPage from "../pages/main-page.js";

export default class MainActions {
    constructor(driver) {
        var mainPage = new MainPage(driver);
        this.mainPage = mainPage;
    }

    waitForPageToLoad() {
        this.mainPage.waitToLoad();
    }

    //TODO: add checks for state of side menu (expanded or not)
    /**
     * Expands `Build` side menu and clicks `Processes` item.
     * Warning. It is expected `Build` side menu to be not expanded when calling this method. Otherwise method behavior
     * will not be correct relatively to the purpose.
     * @see {@link clickProcessesItem()}
     */
    openProcesses(){
        this.mainPage.clickBuildGroupButton();
        this.mainPage.waitForBuildMenu();
        this.mainPage.clickProcessesItem();
    }

    //TODO: add checks for state of side menu (expanded or not)
    openAccess() {
        this.mainPage.clickAccessMenuItem();
    }

    //TODO: add checks for state of side menu (expanded or not)
    openDashboard() {
        this.mainPage.clickWorkGroupButton();
        this.mainPage.waitForWorkDropDown();
        this.mainPage.clickDashboardItem();
    }
    //TODO: remove this when checks for side menus will be up
    /**
     * Clicks `Build` side menu's `Processes` item.
     * Warning. It is expected `Build` side menu to be already expanded when calling this method. Otherwise method
     * behavior will not be correct relatively to the purpose.
     * @see {@link openProcesses()}
     */
    clickProcessesItem() {
        this.mainPage.clickProcessesItem();
    }
};

