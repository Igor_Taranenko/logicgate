import webdriver from 'selenium-webdriver';
import ProcessPage from "../pages/process-page.js";


/**
 * Chain actions provider for the {@link ProcessPage} class.
 * @see {@link WebActions}
 */
export default class ProcessActions {
    /**
     * Sole constructor.
     * @param {webdriver.driver} driver Web driver instance to use.
     */
    constructor(driver) {
        var processPage = new ProcessPage(driver);
        this.processPage = processPage;
    }

    /**
     * Waits for process details page to load.
     */
    waitForPageToLoad(){
        this.processPage.waitToLoad();
    }

    isProcessPageLoaded(processName){
        return this.processPage.isProcessPageLoaded(processName);
    }

    isWorkflowCanvasDisplayed() {
        return this.processPage.isWorkflowCanvasDisplayed();
    }

    /**
     * @return {promise<boolean>} promise if new workflow created
     */
    isWorkflowCreated() {
        return this.processPage.isWorkflowCreated();
    }

    /**
     * @return {promise<string>} name of opened workflow
     */
    checkAdminScreenForWorkflow() {
        return this.processPage.getWorkflowNameFromAdminScreen();
    }

    /**
     * Sets name and object name for new workflow
     * @param {string} workflowName 
     * @param {string} objectName 
     */
    createWorkflow(workflowName, objectName) {
        this.processPage.shiftAndClickWorkflowCanvas();
        this.processPage.waitForModalWorkflowPopupToAppear();
        this.processPage.setWorkflowNameModal(workflowName);
        this.processPage.setWorkflowObjectNameModal(objectName);
        this.processPage.clickCreateWorkflowButton();
    }

    editWorkflowInModal(newWorkflowName, newObjectName) {
        this.processPage.rightClickWorkflowArea();
        this.processPage.clickContextMenuEditItem();
        this.processPage.waitForModalWorkflowPopupToAppear();
        this.processPage.setWorkflowNameModal(newWorkflowName);
        this.processPage.setWorkflowObjectNameModal(newObjectName);
        this.processPage.clickSaveChangesButton();
    }

    checkForEditsToApply() {
        return this.processPage.getWorkflowNameFromCanvas();
    }

    /**
     * Opens created workflow 
     */
    openWorkflowAdminScreen() {
        this.processPage.doubleClickWorkflowArea();
        this.processPage.waitForAdmeenScreeToLoad();
    }

    editWorkflowInAdminScreen(newWorkflowName, newObjectName) {
        this.processPage.setWorkflowName(newWorkflowName);
        this.processPage.setWorkflowObjectName(newObjectName);
        this.processPage.clickProcessBreadcrumb();
    }

    createNode(nodeName) {
        this.processPage.shiftAndClickWorkflowArea();
        this.processPage.setNodeName(nodeName);
        this.processPage.clickNodeArea(nodeName);
    }

    /**
     * Opens edit form for node with specified name
     * @param {string} nodeName node name to open menu
     */
    openNodeEditForm(nodeName){
        this.processPage.rightClickNodeArea(nodeName);
        this.processPage.clickContextMenuEditItem();
        this.processPage.waitForNodeModalPopupToAppear(nodeName);
    }
    /**
     * Will set new name for a node via shift-and-clicking it
     * @param {string} currentName current node name to locate it
     * @param {string} newName new name of the node
     */
    renameNodeNameViaShiftClick(currentName, newName){
        this.processPage.shiftAndClickNode(currentName);
        this.processPage.setNodeName(newName);
    }
    renameNodeViaModalForm(newNodeName) {
        this.processPage.setNodeModalName(newNodeName);
        this.processPage.clickNodeModalSubmitChanges();
    }

    /**
     * Checks if node with specified name exists
     * @param {string} nodeName name of the node to check
     * @return {boolean} true if node exists or times out if not
     */
    isNodeWithNameExists(nodeName) {
        return this.processPage.isNodeCreated(nodeName);
    }

    dragAndDropNode(nodeName, xOffset, yOffset){
        this.processPage.dragAndDropNode(nodeName, xOffset, yOffset);
    }

    /**
     * Creates transition between two nodes
     * @param {string} origin name of the origin node
     * @param {string} end name of the end node
     */
    createTransitionBetweenNodes(origin, end){
        this.processPage.createTransitionBetweenNodes(origin, end);
    }

    editNodeRoleAndOriginStatus(){
        this.processPage.clickNodeRoleUncheckedPencilIcon();
        // this.processPage.switchOriginToggler();
        this.processPage.clickNodeModalSubmitChanges();
    }

    /**
     * Retrieves process name.
     * @returns {Promise} Promise object containing text.
     */
    getProcessName() {
        return this.processPage.getProcessName();
    }
};
