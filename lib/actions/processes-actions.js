import webdriver from 'selenium-webdriver';
import ProcessesPage from "../pages/processes-page.js";
import settings from '../../config/settings.json';

export default class ProcessesActions {
    constructor(driver) {
        var processesPage = new ProcessesPage(driver);
        this.processesPage = processesPage;
    }

    waitForPageToLoad(){
        this.processesPage.waitToLoad();
    }

    createNewProcess(name){
        this.processesPage.clickCreateProcessButton();
        this.processesPage.waitForNewProcessPopup();
        this.processesPage.setProcessName(name);
        this.processesPage.clickConfirmNewProcessButton();
    }

    openProcess(processName){
        this.processesPage.clickProcessItem(processName);
    }
    // TODO: Remove
    /**
     * Retrieves last created process name.
     * @param {string} processName Process name to find the last occurrence of.
     * @returns {Promise} Promise object containing text.
     */
    getLastCreatedProcessName(processName) {
        return this.processesPage.getLastCreatedProcessName(processName);
    }

    /**
     * Clicks last created process, with the name given by `processName` argument, edit icon.
     * @param {string} processName Process name to click the last occurrence of.
     */
    clickProcessEditIcon(processName) {
        this.processesPage.clickProcessEditIcon(processName);
    }

    /**
     * Clicks color picker button from process details pop-up which can be triggered by
     * {@link clickLastCreatedProcessEditIcon()}.
     */
    clickColorPicker() {
        this.processesPage.clickColorPicker();
    }

    /**
     * Clicks color hue meter at specified y-point, thus setting color hue 
     * @param {number} yOffset offset on y axis in pixels relative to the hue picker.
     *  `Accepted values`: [1,99] 
     */
    pickColorHue(yOffset) {
        this.processesPage.setColorPickerHue(yOffset);   
    }

    /**
     * Clicks color picker at specified point, thus setting color
     * @param {number} xOffset offset on x axis in pixels relative to the color picker.
     *  `Accepted values`: [1,99]
     * @param {number} yOffset offset on y axis in pixels relative to the color picker.
     *  `Accepted values`: [1,99]
     */
    pickColor(xOffset, yOffset) {
        this.processesPage.setColorPickerColor(xOffset, yOffset);           
    }

    changeProcessIcon(){
        this.processesPage.clickIconDropdownButton();
        this.processesPage.clickExclamationIcon();
    }
    /**
     * Clicks `Done` button from the pop-up triggered by `Edit` process button.
     */
    clickEditPopUpDoneButton() {
        this.processesPage.clickEditPopUpDoneButton();
    }

    getProcessColor(processName){
        return this.processesPage.getProcessColor(processName);
    }

    checkForIconChanges(processName){
        return this.processesPage.isInfoBoxIconExclamation(processName);
    }
};
