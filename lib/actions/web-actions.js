import LoginActions from './login-actions.js';
import MainActions from './main-actions.js';
import ProcessesActions from './processes-actions.js';
import ProcessActions from './process-actions.js';
import AccessActions from './access-actions.js';

export default class WebActions {
    constructor(driver){
        this.loginActions = new LoginActions(driver);
        this.mainActions = new MainActions(driver);
        this.processesActions = new ProcessesActions(driver);
        this.processActions = new ProcessActions(driver);
        this.accessActions = new AccessActions(driver);
    }
};

