/**
 * Base class for all locators.
 */
export default class BaseLocator {
    /**
     * Returns locator stored by the instance.
     * Note. This method should be overriden by extending class.
     * @param {arguments} args Formatting sequences if any.
     * @returns {WebElement} Web element represented by locator.
     */
    getLocator(...args) {}
};
