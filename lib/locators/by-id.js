import BaseLocator from './base-locator.js';
import webdriver from 'selenium-webdriver';
import util from 'util';


/**
 * Locates elements by the ID attribute.
 */
export default class ById extends BaseLocator {

    /**
     * Sole constructor.
     * @param {string} locator Locator pattern to be represented. May be a format string.
     */
    constructor(locator) {
        super();
        this.locator = locator;
    }


    /**
     * @override
     */
    getLocator(...args) {
        return webdriver.By.id(util.format(this.locator, ...args));
    }
};
