import BaseLocator from './base-locator.js';
import webdriver from 'selenium-webdriver';
import util from 'util';


/**
 * Locates link elements whose visible text matches the given string.
 */
export default class ByLinkText extends BaseLocator {

    /**
     * Sole constructor.
     * @param {string} locator Locator pattern to be represented. May be a format string.
     */
    constructor(locator) {
        super();
        this.locator = locator;
    }


    /**
     * @override
     */
    getLocator(...args) {
        return webdriver.By.linkText(this.locator, ...args);
    }
};
