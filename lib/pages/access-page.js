import webdriver from 'selenium-webdriver';
import BasePage from './base-page.js';
import ByCss from '../locators/by-css.js';
import ByXpath from '../locators/by-xpath.js';

const accessPageTitle = new ByXpath("//h1//*[contains(.,'Access')]");
const roleQA1Item = new ByXpath("//a[contains(@class,'btn-block ellipsis')]//span[contains(text(),'QA1')]");
const rolePopup = new ByCss("form.form-group");
const nodesTab = new ByXpath("//li[contains(@class,'uib-tab')]//a[contains(.,'Nodes')]");
const processItem = new ByXpath("//a//*[contains(text(),'%s')]");
const nodeWithRoleContainer = new ByXpath("//ul[contains(@class,'pretty-list') and .//li[contains(.,'workflow')]]");
const nodeWithRoleItem = new ByXpath("//li[contains(.,'workflow')]//i[contains(@class,'text-green')]");
const addAllNodesButton = new ByXpath("//small//a[@role='button' and contains(.,'%s')]");
const doneButton = new ByCss("button.btn.btn-primary");

export default class AccessPage extends BasePage {
    /**
     * Constructor for a page
     * @param {webdriver.driver} driver 
     */
    constructor (driver) {
        super (driver);     
    }
    waitToLoad(){
        super.waitForElementToBeVisible("Waiting for Access Page to load", accessPageTitle);
    }

    clickRoleQA1Item(){
        super.waitForElementToBeClickable("Waiting for role item 'QA1' to be clickable", roleQA1Item);
        super.click("Clicking 'QA1' role item", roleQA1Item);
    }

    waitForRolePopupToAppear(){
        super.waitForElementToBeVisible("Waiting for role popup of QA1 to appear", rolePopup);
    }

    //TODO: implement this
    // waitForRolePopupToDissapear(){
    //     super.waitForElementToBeVisible("Waiting for role popup of QA1 to appear", rolePopup);
    // }

    clickNodesTab(){
        super.waitForElementToBeClickable("Waiting for 'Nodes' tab to be clickable", nodesTab);        
        super.click("Swithing to 'Nodes' tab", nodesTab);
    }

    clickProcessItem(processName){
        super.waitForElementToBeClickable("Waiting for process " + processName + " to be clickable", processItem, processName);       
        super.click('Clicking process ' + processName, processItem, processName);
    }

    clickAddAllNodesButton(processName){
        super.click("Clicking 'add all nodes in" + processName.toLowerCase()+"'", addAllNodesButton, processName.toLowerCase());
    }

    clickDoneButton(){
        super.click("Clicking 'Done' button of roles modal window", doneButton);
    }
};