import webdriver from 'selenium-webdriver';
import config from '../../config/settings.json';
import input from 'selenium-webdriver/lib/input';
import BaseLocator from '../locators/base-locator.js';
import promise from 'selenium-webdriver/lib/promise';
import WinstonLogger from '../util/logger.js';

const until = webdriver.until;
const logger = new WinstonLogger();

export default class BasePage {
    constructor(driver) {
        this.explicitWaitMS = config.elementTimeOut;
        this.driver = driver;
    }

    /**
     * Will wait for element to be visible for amount of time
     * specified in explisitWaitMS
     * @param {string} logMessage Message to log.
     * @param {BaseLocator} locator Element to wait for visibility of.
     * @param {arguments} args Optional arguments in case of `locator` contains a format string.
     */
    waitForElementToBeVisible(logMessage, locator, ...args) {
        this.logInfo(logMessage);
        let element = this.driver.findElement(locator.getLocator(...args));
        return this.driver.wait(
            until.elementLocated(locator.getLocator(...args)),
            this.explicitWaitMS
        )
            .then(element => {
                return this.driver.wait(
                    until.elementIsVisible(element),
                    this.explicitWaitMS
                );
            });
    }

    /**
     * Waits for given by `locator` element to be available for user interaction.
     * @param {string} logMessage Message to log.
     * @param {BaseLocator} locator Element to wait to be enabled of.
     * @param {arguments} args Optional arguments in case of `locator` contains a format string.
     */
    waitForElementToBeEnabled(logMessage, locator, ...args) {
        this.logInfo(logMessage);
        let element = this.driver.findElement(locator.getLocator(...args));
        return this.driver.wait(until.elementIsEnabled(element), this.explicitWaitMS);
    }

    /**
     * Will wait for element to be clickable for amount of time
     * specified in explisitWaitMS
     * @param {string} logMessage Message to log.
     * @param {BaseLocator} locator Element to wait for visibilty of.
     * @param {arguments} args Optional arguments in case of `locator` contains a format string.
     */
    waitForElementToBeClickable(logMessage, locator, ...args) {
        this.logInfo(logMessage);
        return this.driver.wait(
            until.elementLocated(locator.getLocator(...args)),
            this.explicitWaitMS
        )
            .then(element => {
                return this.driver.wait(
                    until.elementIsEnabled(element),
                    this.explicitWaitMS
                );
            });
    }

    waitForElementToBeStale(logMessage, locator, ...args){
        this.logInfo(logMessage);
        let element = this.driver.findElement(locator.getLocator(...args));
        return this.driver.wait(until.stalenessOf(element), this.explicitWaitMS);
    }

    waitForElementToContainText(logMessage, text, locator, ...args){
        this.logInfo(logMessage);
        let element = this.driver.findElement(locator.getLocator(...args));
        return this.driver.wait(until.elementTextContains(element, text), this.explicitWaitMS);
    }
    /**
     * Used to type specified message into element,
     * described by locator passed
     * @param {string} logMessage Message to log.
     * @param {string} value Value to put into specified by `locator` text field.
     * @param {BaseLocator} locator Element to input text into.
     * @param {arguments} Optional arguments in case of `locator` contains a format string.
     */
    type(logMessage, value, locator, ...args) {
        this.logInfo(logMessage);
        let element = this.driver.findElement(locator.getLocator(...args));
        element.clear();
        element.sendKeys(value);
    }

    /**
     * Clicks the element specified by locator passed in
     * @param {string} logMessage Message to log.
     * @param {BaseLocator} locator Element to click.
     * @param {arguments} args Optional arguments in case of `locator` contains a format string.
     */
    click(logMessage, locator, ...args) {
        this.logInfo(logMessage);
        this.driver.findElement(locator.getLocator(...args)).click();
    }

    /**
     * Performs a right mouse button click on a specified by `locator` element.
     * @param {string} logMessage Message to log.
     * @param {BaseLocator} locator Element to perform a right mouse button click on.
     * @param {arguments} args Optional arguments in case of `locator` contains a format string.
     */
    rightClick(logMessage, locator, ...args) {
        this.logInfo(logMessage);
        let element = this.driver.findElement(locator.getLocator(...args));
        new webdriver.ActionSequence(this.driver)
            .click(element, input.Button.RIGHT)
            .perform();
    }

    /**
     * Performs a left mouse button double click on a specified by `locator` element.
     * @param {string} logMessage Message to log.
     * @param {BaseLocator} locator Element to perform a left mouse button double click on.
     * @param {arguments} args Optional arguments in case of `locator` contains a format string.
     */
    doubleClick(logMessage, locator, ...args) {
        this.logInfo(logMessage);
        let element = this.driver.findElement(locator.getLocator(...args));
        new webdriver.ActionSequence(this.driver)
            .doubleClick(element)
            .perform();
    }

    /**
     * Performs a left mouse button click on a specified by `locator` element with the `Shift` key being held down.
     * @param {string} logMessage Message to log.
     * @param {BaseLocator} locator Element to perform a left mouse button click with the `Shift` key being held down.
     * @param {arguments} args Optional arguments in case of `locator` contains a format string.
     */
    shiftAndClick(logMessage, locator, ...args) {
        this.logInfo(logMessage);
        let element = this.driver.findElement(locator.getLocator(...args));
        new webdriver.ActionSequence(this.driver)
            .keyDown(webdriver.Key.SHIFT)
            .click(element)
            .keyUp(webdriver.Key.SHIFT)
            .perform();
    }

    /**
     * Determines whether specified by `locator` element is displayed on a web page.
     * @param {string} logMessage Message to log.
     * @param {BaseLocator} locator Element to determine visibility of.
     * @param {arguments} args Optional arguments in case of `locator` contains a format string.
     * @returns {boolean} true - specified by `locator` element is visible; false otherwise.
     */
    isElementDisplayed(logMessage, locator, ...args) {
        this.logInfo(logMessage);
        let element = this.driver.findElement(locator.getLocator(...args));
        return element.isDisplayed();
    }

    /**
     * Retrieves given by `locator` element text, including its sub-elements without any leading or trailing
     * whitespace.
     * @param {string} logMessage Message to log.
     * @param {BaseLocator} locator Element to retrieve text from.
     * @param {arguments} args Optional arguments in case of `locator` contains a format string.
     * @returns {Promise} Promise object containing text.
     */
    getElementText(logMessage, locator, ...args) {
        this.logInfo(logMessage);
        let element = this.driver.findElement(locator.getLocator(...args));
        return element.getText();
    }

    /**
     * Retrieves given by `locator` element's attribute value
     * @param {string} logMessage Message to log.
     * @param {BaseLocator} locator Element to retrieve value from.
     * @param {string} attribute Attribute to retrieve value from.
     * @param {arguments} args Optional arguments in case of `locator` contains a format string.
     * @returns {Promise} Promise object containing value of passed attribute.
     */
    getElementCssAttribute(logMessage, locator, attribute, ...args){
        this.logInfo(logMessage);
        let element = this.driver.findElement(locator.getLocator(...args));
        return element.getCssValue(attribute);
    }
    /**
     * Will wait for specified amount in miliseconds
     * @param {number} waitMs Amount of time to wait in milliseconds
     */
    wait(waitMs) {
        this.driver.sleep(waitMs);
    }

    /**
     * Drags element by specified offset.
     * @param {string} logMessage Message to log.
     * @param {BaseLocator} locator Locator for element to move. 
     * @param {number} xOffset Offset for x axis in pixels.
     * @param {number} yOffset Offset for y axis in pixels.
     * @param {arguments} args Optional arguments in case of `locator` contains a format string. 
     */
    dragAndDropElementByOffset(logMessage, locator, xOffset, yOffset, ...args) {
        this.logInfo(logMessage);
        let element = this.driver.findElement(locator.getLocator(...args));
        new webdriver.ActionSequence(this.driver)
            .dragAndDrop(element, { x: xOffset, y: yOffset })
            .perform();
    }

    /**
     * Performs `Shift+Click` operation on element without releasing the key and mouse button
     * @param {string} logMessage Message to log.
     * @param {BaseLocator} locator Locator for element to perform actions.
     * @param {arguments} args Optional arguments in case of `locator` contains a format string. 
     */
    shiftAndClickWithoutRelease(logMessage, locator, ...args) {
        this.logInfo(logMessage);        
        let element = this.driver.findElement(locator.getLocator(...args));
        new webdriver.ActionSequence(this.driver)
            .keyDown(webdriver.Key.SHIFT)
            .mouseDown(element)
            .perform();
    }

    /**
     * Releases `Shift` key and `LMB` locked by call of {@link shiftAndClickWithoutRelease}
     * @param {string} logMessage Message to log.
     * @param {BaseLocator} locator Locator for element to perform actions.
     * @param {arguments} args Optional arguments in case of `locator` contains a format string. 
     */
    releaseShiftAndClickOnElement(logMessage, locator, ...args) {
        this.logInfo(logMessage);        
        let element = this.driver.findElement(locator.getLocator(...args));
        new webdriver.ActionSequence(this.driver)
            .keyUp(webdriver.Key.SHIFT)
            .mouseUp(element)
            .perform();
    }

    /**
     * Logs a specified message as info
     * @param {string} logMessage 
     */
    logInfo(logMessage) {
        logger.logInfo(logMessage);
    }

    /**
     * Schedules a command to execute JavaScript in the context of the currently selected frame or window. The script
     * fragment will be executed as the body of an anonymous function.
     * Note. The script passes element provided by `locator` as a single argument to the JavaScript function call.
     * @param {string} logMessage Message to log.
     * @param {string} script JavaScript to execute.
     * @param {BaseLocator} locator Element to be passed as argument to JavaScript executor.
     * @param {arguments} args Optional arguments in case of `locator` contains a format string.
     * @returns {Promise}
     */
    executeScript(logMessage, script, locator, ...args) {
        this.logInfo(logMessage);
        return this.driver.executeScript(script, this.driver.findElement(locator.getLocator(...args)));
    }

    /**
     * Clicks an element at point, specified by offsets. This offsets are in pixels
     *  and are relative to the elements left top corner.
     * @param {string} logMessage Message to log
     * @param {BaseLocator} locator Element to be clicked.
     * @param {number} xOffset offset on x axis in pixels
     * @param {number} yOffset offset on y axis in pixels
     * @param {arguments} args Optional arguments in case of `locator` contains a format string.
     */
    clickElementAtPoint(logMessage, locator, xOffset, yOffset, ...args){
        this.logInfo(logMessage);
        let element = this.driver.findElement(locator.getLocator(...args));
        new webdriver.ActionSequence(this.driver)
            .mouseMove(element, {x: xOffset, y: yOffset})
            .click()
            .perform();
    }
};
