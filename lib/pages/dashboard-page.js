import webdriver from 'selenium-webdriver';
import BasePage from './base-page.js';
import ByCss from '../locators/by-css.js';
import ByXpath from '../locators/by-xpath.js';

export default class DashboardPage extends BasePage {
    /**
     * Constructor for a page
     * @param {webdriver.driver} driver 
     */
    constructor (driver) {
        super (driver);     
    }
    waitToLoad(){
        super.waitForElementToBeVisible("Waiting for Access Page to load", );
    }
};