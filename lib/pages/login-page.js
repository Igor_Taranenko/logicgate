import webdriver from 'selenium-webdriver';
import BasePage from './base-page.js';
import ByCss from '../locators/by-css.js';

const loginForm = new ByCss("div.register-box-body");
const usernameInput = new ByCss("input[ng-model='credentials.email']");
const passwordInput = new ByCss("input[ng-model='credentials.password']");
const loginButton = new ByCss("button[type='submit']");

export default class LoginPage extends BasePage {
     /**
     * Constructor for a page
     * @param {webdriver.driver} driver 
     */
    constructor( driver ) {
        super(driver);
    }

    /**
     * Waits for Login Page to load by waiting
     * for login form visibility
     */
    waitToLoad() {
        super.waitForElementToBeVisible('Wait for login form to be visible', loginForm);
    }

    /**
     * Clicks login button
     */
    clickLoginButton(){
        super.click('Click login button', loginButton);
    }

    /**
     * Types provided username parameter to username input
     * @param {string} username 
     */
    setUsername(username){
        super.type('Put `' + username + '` into username text field', username, usernameInput);
    }

    /**
     * Types provided password parameter to password input
     * @param {string} password 
     */
    setPassword(password){
        super.type('Put `' + password + '` into password text field', password, passwordInput);
    }

};
