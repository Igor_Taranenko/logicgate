import webdriver from 'selenium-webdriver';
import BasePage from './base-page.js';
import ByCss from '../locators/by-css.js';
import ByXpath from '../locators/by-xpath.js';

const navBar = new ByCss("nav.navbar-static-top");
const buildGroupButton = new ByCss("i.fa.fa-puzzle-piece");
const buildGroupList = new ByXpath("//ul[contains(.,'Build')]//ul[contains(@class,'in collapse')]");
const processesItem = new ByXpath("//a[contains(text(),'Processes')]");
const accessItem = new ByXpath("//a[contains(text(),'Access')]"); 
const workGroupButton = new ByCss("i.fa.fa-dashboard");
const workGroupList = new ByXpath("//ul[contains(.,'Work')]//ul[contains(@class,'in collapse')]");
const dashboardItem = new ByXpath("//a[contains(text(),'Dashboard')]");
export default class MainPage extends BasePage {
    /**
     * Constructor for a page
     * @param {webdriver.driver} driver 
     */
    constructor (driver) {
        super (driver);     
    }

    /**
     * Waits for Main Page to load by waiting
     * for login form visibility
     */
    waitToLoad() {
        super.waitForElementToBeVisible("Waiting for navigation bar of Main Page to load", navBar);
    }
    
    /**
     * Clicks on 'Build' drop-down list
     */
    clickBuildGroupButton(){
        super.waitForElementToBeClickable('Waitinig for `Build` drop-down list', buildGroupButton);        
        super.click("Clicking on 'Build' drop-down list", buildGroupButton);
    }

    /**
     * Waits for `Build` menu to appear
     */
    waitForBuildMenu(){
        super.waitForElementToBeVisible("Waiting for drop-down list for Build Group to load", buildGroupList);
    }

    /**
     * Clicks `Processes` item of `Build` menu
     */
    clickProcessesItem(){
        super.waitForElementToBeClickable('Waitinig for `Processes` build menu item to appear', processesItem);
        super.click('Click `Processes` build menu item', processesItem);
    }

     /**
     * Clicks `Access` item of `Build` menu
     */
    clickAccessMenuItem(){
        super.waitForElementToBeClickable('Waitinig for `Access` build menu item to appear', accessItem);
        super.click('Click `Access` build menu item', accessItem);
    }

    /**
     * Clicks `Work` 
     */
    clickWorkGroupButton(){
        super.waitForElementToBeClickable("Waiting for 'Work' drop-down list to be clickable", workGroupButton);
        super.click("Clicking 'Work' drop-down button", workGroupButton);
    }

    waitForWorkDropDown(){
        super.waitForElementToBeVisible("Waiting for drop-down list for Work Group to load", workGroupList);
    }

    clickDashboardItem(){
        super.waitForElementToBeClickable('Waitinig for `Dashboard` work menu item to appear', dashboardItem);
        super.click("Clicking 'Dashboard' item", dashboardItem);        
    }
};