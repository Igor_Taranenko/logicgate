import webdriver from 'selenium-webdriver';
import BasePage from './base-page.js';

import ByXpath from '../locators/by-xpath.js';
import ByCss from '../locators/by-css.js';
import data from '../../config/test-case-data.json';

const title = new ByXpath("//section[contains(@class, 'content-header') and .//span[text()='%s']]");
const workflowCanvas = new ByCss("div#workflows-graph-wrapper");
const modalWorkflowPopup = new ByCss("div.modal-body");
const modalWorkflowNameInput = new ByCss("input#modalWorkflow");
const modalWorkflowObjectNameInput = new ByCss("input#modalObject");
const createWorkflowButton = new ByCss("a.btn.btn-success");

// Process name header
const processName = new ByXpath("//div[@id='app-content-wrapper']//span[@ng-if]");

const workflowArea = new ByCss("div#workflows-graph-wrapper g.box-g");
const workflowNameLabel = new ByCss("g.box-g text");
const defaultOriginNode = new ByCss("g.node-g .origin");
const defaultEndNode = new ByCss("g.node-g .end");

const contextMenuEditItem = new ByXpath("//li[text()='Edit']");
const saveChangesButton = new ByXpath("//a[contains(@class,'btn-primary')]//span[text()='%s']");

const adminScreenHeader = new ByXpath("//h1//span[contains(text(),'%s')]");
const adminScreenWorkflowName = new ByXpath("//ol[contains(@class,'breadcrumb')]//span[contains(@class,'ng-scope')]");
const workflowNameInput = new ByCss("input#workflow");
const workflowObjectNameInput = new ByCss("input#objectName");
const breadcrumbPart = new ByXpath("//ol[contains(@class,'breadcrumb')]//span[contains(text(),'%s')]");

const nodeNameInput = new ByCss("p#active-editing");
const nodeAreaCircle = new ByXpath("//*[contains(@class,'node-g') and .//node()[contains(.,'%s')]]");
const nodeTextLabel = new ByXpath("//*[contains(@class,'node-g')]//node()[contains(., '%s')]");
const nodeModalTitle = new ByXpath("//form[@name='modalNodeForm']//span[text()[contains(.,'%s')]]");
const nodeModalNameInput = new ByCss("input#modalNode");
const nodeModalSubmitButton = new ByCss("button[type='submit']");

const nodeRoleUncheckedPencilIcon = new ByXpath("//li[div[text()[contains(.,'%s')]]]//i[@class[contains(.,'text-gray')]]");
const nodeRoleCheckedPencilIcon = new ByXpath("//li[div[text()[contains(.,'%s')]]]//i[@class[contains(.,'text-green')]]");
const nodeOriginToggler = new ByXpath("//label[.//span[text()[contains(.,'Origin')]]]//div");


/**
 * Describes process details page.
 * @see {@link ProcessActions}
 */
export default class ProcessPage extends BasePage {
    /**
     * Constructor for a page
     * @param {webdriver.driver} driver Web driver instance.
     */
    constructor (driver) {
        super (driver);
    }

    /**
     * Waits for Main Page to load by waiting
     * for login form visibility
     */
    waitToLoad() {
        super.waitForElementToBeVisible("Waiting for title of Process page to load", title, 'Process');
    }

    isProcessPageLoaded(processName){
        return super.isElementDisplayed("Checking if process " + processName + " is opened.", breadcrumbPart, processName);
    }

    isWorkflowCanvasDisplayed(){
        super.waitForElementToBeVisible("Waiting for workflow canvas to be visible", workflowCanvas);
        return super.isElementDisplayed("Checking for workflow canvas to be visible", workflowCanvas);
    }

    isWorkflowCreated(){
        let nodeOrigin = super.isElementDisplayed("Checking for visibility of origin node", defaultOriginNode);
        let nodeEnd = super.isElementDisplayed("Checking for visibility of end node", defaultEndNode);
        return nodeOrigin && nodeEnd;
    }

    waitForAdmeenScreeToLoad(){
        super.waitForElementToBeVisible("Waiting for admin screen to load", adminScreenHeader, 'Workflow');
    }

    shiftAndClickWorkflowCanvas(){
        super.waitForElementToBeClickable("Waiting for workflow canvas to be clickable", workflowCanvas);
        super.shiftAndClick('Shift and clicking on workflowCanvas', workflowCanvas)
    }
    
    waitForModalWorkflowPopupToAppear(){
        super.waitForElementToBeVisible("Waiting for 'Workflow' popup to appear", modalWorkflowPopup);
    }

    setWorkflowNameModal(workflowName){
        super.type("Setting 'Workflow name' to " + workflowName, workflowName, modalWorkflowNameInput);
    }

    setWorkflowObjectNameModal(objectName){
        super.type("Setting 'Object Name' to " + objectName, objectName, modalWorkflowObjectNameInput);        
    }

    clickCreateWorkflowButton(){
        super.click("Clicking 'Create' button", createWorkflowButton);
    }

    rightClickWorkflowArea(){
        super.waitForElementToBeClickable("Waiting for workflow area to be clickable", workflowArea);
        super.rightClick('Right-clicking newly created workflow area', workflowArea);
    }

    clickContextMenuEditItem(){
        super.waitForElementToBeVisible("Waiting for 'Edit' item to appear", contextMenuEditItem);
        super.click("Clicking 'Edit' item of context menu", contextMenuEditItem);
    }

    clickSaveChangesButton(){
        super.click("Clicking 'Save' button of modal edit form", saveChangesButton, 'Save');
    }

    doubleClickWorkflowArea(){
        super.waitForElementToBeClickable("Waiting for workflow area to be clickable", workflowArea);
        super.doubleClick('Double-clicking edited workflow area', workflowArea);
    }

    setWorkflowName(workflowName){
        super.type("Setting 'Workflow name' to " + workflowName, workflowName, workflowNameInput);
    }

    setWorkflowObjectName(objectName){
        super.type("Setiing 'Object name' to "+ objectName, objectName, workflowObjectNameInput);
    }

    clickProcessBreadcrumb(){
        super.wait(1000);     //essential wait, without it UI won't update for canvas   
        super.click("Clicking breadcrumb for this process", breadcrumbPart, data.processName);
    }

    getWorkflowNameFromCanvas(){
        super.waitForElementToBeVisible('Waiting for name of workflow to be visible', workflowNameLabel);
        return super.getElementText('Retrieve workflow name from canvas', workflowNameLabel);
    }

    //TODO: check this wait
    getWorkflowNameFromAdminScreen(){
//        super.waitForElementToBeVisible("Waiting for title of workflow to appear in ", contextMenuEditItem);
        return super.getElementText('Retrieve workflow name from admin screen', adminScreenWorkflowName);
    }

    shiftAndClickWorkflowArea(){
        super.waitForElementToBeClickable("Waiting for workflow area to be clickable", workflowArea);
        super.shiftAndClick('Shift and clicking on workflow area', workflowArea);
    }

    setNodeName(nodeName){
        super.waitForElementToBeClickable("Waiting for node's name input to be clickable", nodeNameInput);
        super.type('Setting new nodes name to ' + nodeName, nodeName, nodeNameInput);
        super.click('Clicking workflow to apply new name to node', workflowArea);       
    }

    clickNodeArea(nodeName){
        //call ".replace(/\s/g,'')" reqired because of internal representation of node's text that does not contain whitespace
        super.click('Clicking node: ' + nodeName, nodeAreaCircle, nodeName.replace(/\s/g,''));
    }
    rightClickNodeArea(nodeName){
        //call ".replace(/\s/g,'')" reqired because of internal representation of node's text that does not contain whitespace
        super.rightClick('Right-clicking node: '+ nodeName, nodeAreaCircle, nodeName.replace(/\s/g,'')); 
    }

    waitForNodeModalPopupToAppear(nodeName){
        super.waitForElementToBeVisible('Waiting for modal form for node editing to apper', nodeModalTitle, nodeName);
    }

    setNodeModalName(nodeName){
        super.type('Setting ' + nodeName + ' as new nodes name', nodeName, nodeModalNameInput);
    }

    clickNodeModalSubmitChanges(){
        super.click("Clicking 'Submit' button of nodes edit modal form", nodeModalSubmitButton);
    }

    isNodeCreated(nodeName){
        super.waitForElementToBeVisible("Waiting for node '"+ nodeName + "' to be clickable",
            nodeTextLabel, nodeName.replace(/\s/g,''));          //call ".replace(/\s/g,'')" reqired because of internal representation of node's text that does not contain whitespace
        return super.isElementDisplayed('Checking if nide with name '+ nodeName + ' is Displayed', nodeTextLabel, nodeName.replace(/\s/g,''));
    }

    shiftAndClickNode(nodeName){
        super.waitForElementToBeClickable("Waiting for node '"+ nodeName + "' to be clickable",
        nodeAreaCircle, nodeName.replace(/\s/g,''));
        super.shiftAndClick('Shift-and-clicking node with name ' + nodeName, nodeAreaCircle, nodeName.replace(/\s/g,''));
    }

    dragAndDropNode(nodeName, xOffset, yOffset){
        super.waitForElementToBeClickable("Waiting for node '"+ nodeName + "' to be clickable",
            nodeAreaCircle, nodeName.replace(/\s/g,''));             
        super.dragAndDropElementByOffset('Drag-and-droping node with name ' + nodeName + ' by x:' + xOffset + ',y:' + yOffset,
         nodeAreaCircle, xOffset, yOffset, nodeName.replace(/\s/g,''));
    }

    createTransitionBetweenNodes(origin, end){
        super.waitForElementToBeClickable("Waiting for node '"+ origin + "' to be clickable",
            nodeAreaCircle, origin.replace(/\s/g,''));
        super.shiftAndClickWithoutRelease(nodeAreaCircle, origin.replace(/\s/g,''));      
        super.releaseShiftAndClickOnElement(nodeAreaCircle, end.replace(/\s/g,''));
    }

    clickNodeRoleUncheckedPencilIcon(){
        super.waitForElementToBeClickable('Waiting for element to be clickable', nodeRoleUncheckedPencilIcon, 'QA1');
        super.click("Clicking pencil icon for role 'QA1'", nodeRoleUncheckedPencilIcon, 'QA1');
    }

    isNodeRoleChecked(){
        return isElementDisplayed("Checking if 'QA1' role for node is checked", nodeRoleCheckedPencilIcon, 'QA1');
    }

    switchOriginToggler(){
        super.click("Switching 'Origin' toggle", nodeOriginToggler);
    }

    /**
     * Retrieves process name.
     * @returns {Promise} Promise object containing text.
     */
    getProcessName() {
        super.waitForElementToBeVisible('', processName);
        return super.getElementText('Retrieve process name', processName);
    }
};
