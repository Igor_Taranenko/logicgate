import webdriver from 'selenium-webdriver';
import BasePage from './base-page.js';
import ByCss from '../locators/by-css.js';
import ByXpath from '../locators/by-xpath.js';
import data from '../../config/test-case-data.json';


const title = new ByXpath("//section[contains(@class, 'content-header') and .//span[text()='Processes']]");
const newProcessButton = new ByCss("div.font-light.text-muted.text-center");
const newProcessPopup = new ByCss("div.modal-content");
const processNameInput = new ByCss("input#process");
const confirmNewProcessButton = new ByXpath(
    "//div[@class='modal-content']//button[contains(@class, 'btn-primary') and ./span[text()='Create']]"
);
/**Last created process item with the given name. Because system allows creation of several processes
 * with the same name, this will pick the last one created, ie created by this script */
const processItem = new ByXpath("(//h4[@class='info-box-title' and ./a[text()='%s']])[last()]");
/** Last created process `Edit` icon. Note this is not a web object. This is a plain string since it has a formatting
 * sequence placeholder. The placeholder determines process name to find the last occurrence of. */
const processEditIcon = new ByXpath(
    "(//h4[@class='info-box-title' and ./a[text()='%s']])[last()]" +
    "/following-sibling::div//i[contains(@class, 'fa-edit')]"
);

/** Color picker button from process details pop-up which can be triggered by clicking on
 * the {@link lastCreatedProcessEditIcon}. */
const colorPicker = new ByXpath("//div[@class='modal-content']//div[@ng-attr-style]");
/** Color picker saturation. */
const colorPickerSaturation = new ByXpath("//div[contains(@class, 'colorpicker')]//colorpicker-saturation");
/** Color picker saturation pointer. */
const colorPickerHue = new ByXpath("//div[contains(@class, 'colorpicker')]//colorpicker-hue");
const editIconDropdownButton = new ByXpath("//div[@class='dropdown' and .//preceding::label[contains(text(),'Icon')]]");
const editExclamationIcon = new ByCss("ul i.fa-exclamation-triangle");
/** `Done` button from the pop-up triggered by `Edit` process button. */
const editPopUpDoneButton = new ByXpath("//div[@class='modal-content']//button[@ng-click='$ctrl.save();']");

const processExclamationIcon = new ByXpath(
    "(//div[contains(@class, 'overlay-wrapper') and .//a[text()='%s']])" +
    "[last()]//i[contains(@class,'fa-exclamation-triangle')]");

const processInfoBox = new ByXpath(
    "(//div[contains(@class, 'overlay-wrapper') and .//a[text()='%s']])" + 
    "[last()]//a[@class='info-box-icon']"
);

export default class ProcessesPage extends BasePage {
    /**
     * Constructor for a page
     * @param {webdriver.driver} driver 
     */
    constructor(driver) {
        super(driver);
        this.driver = driver;
    }

    waitToLoad() {
        super.waitForElementToBeVisible("Waiting for 'Processes' page to load", title);
    }

    clickCreateProcessButton() {
        super.waitForElementToBeClickable("Waiting for 'New Process' button to be enabled", newProcessButton);
        super.click("Clicking 'New Process' button", newProcessButton);
    }

    waitForNewProcessPopup() {
        super.waitForElementToBeVisible("Waiting for 'New Process' modal form to appear", newProcessPopup);
    }

    /**
     * Sets new process name
     * @param {string} name 
     */
    setProcessName(name) {
        super.waitForElementToBeClickable("Waiting for 'Process name' input to be clickable", processNameInput);
        super.type("Setting new process name to be: " + name, name, processNameInput);
    }

    clickConfirmNewProcessButton() {
        super.waitForElementToBeClickable("Waiting for 'Create' button of 'New process' modal window to be clickable", confirmNewProcessButton);
        super.click("Clicking 'Create' button of 'New process' modal window", confirmNewProcessButton);
    }

    /**
     * Clicks last created process, with the name given by `processName` argument, edit icon.
     * @param {string} processName Process name to click the last occurrence of.
     */
    clickProcessEditIcon(processName) {
        super.waitForElementToBeClickable("Waiting for 'Edit' icon to be visible for process:" + processName, processEditIcon, processName);
        super.click('Click last created process', processEditIcon, processName);
    }

    /**
     * Clicks color picker button from process details pop-up which can be triggered by
     * {@link clickLastCreatedProcessEditIcon(string)}.
     */
    clickColorPicker() {
        super.waitForElementToBeClickable('Waiting for color picker button of the process details pop-up to be clickable', colorPicker)
        super.click('Click color picker button of the process details pop-up', colorPicker);
    }

    clickProcessItem(processName) {
        super.waitForElementToBeStale('', newProcessPopup);
        super.waitForElementToBeClickable("Waiting for process item '" + processName + "' to be clickable", processItem, processName);
        super.click("Clicking process item with name '" + processName + "'", processItem, processName);
    }

    /**
     * Clicks color-pickers hue at point specified relative to 
     * left-top corner of the meter, thus setting hue of the color-picker
     * @param {number} yOffset offset for y in pixels, counting from top left corner of the element
     */
    setColorPickerHue(yOffset) {
        if ( yOffset < 0 || yOffset > 100) {
            throw new Error("Invalid coordinates of y: " + yOffset + " have been passed");
        }
        super.waitForElementToBeClickable('Waiting for hue meter of color picker to be clickable', colorPickerHue)
        super.clickElementAtPoint('Clicking hue meter at point: ' + yOffset,
            colorPickerHue, 0, yOffset);
    }

    /**
     * Clicks color-picker at point specified relative to 
     * left-top corner of the color-picker dialog, thus setting color of the color-picker
     * @param {number} xOffset 
     * @param {number} yOffset 
     */
    setColorPickerColor(xOffset, yOffset) {
        if (xOffset < 0 || xOffset > 100 || yOffset < 0 || yOffset > 100) {
            throw new Error("Invalid coordinates of x: " + xOffset + ", y: " + yOffset + " have been passed");
        }
        super.waitForElementToBeClickable('Waiting for color picker to be clickable', colorPickerSaturation)
        super.clickElementAtPoint('Clicking hue meter at point: ' + xOffset + ';' + yOffset,
            colorPickerSaturation, xOffset, yOffset);
    }

    clickIconDropdownButton(){
        super.waitForElementToBeClickable("Waiting for 'Icon' selection button to be clickable", editIconDropdownButton);
        super.click("Clicking 'Icon' selection button", editIconDropdownButton);
    }

    clickExclamationIcon(){
        super.waitForElementToBeClickable("Waiting for 'Exclamation' triangle icon to be clickable", editExclamationIcon);
        super.click("Clicking 'Exclamation' triangle icon", editExclamationIcon);
    }

    /**
     * Clicks `Done` button from the pop-up triggered by `Edit` process button.
     */
    clickEditPopUpDoneButton() {
        super.click('Click `Done` button from the pop-up triggered by `Edit` process button', editPopUpDoneButton);
    }

    getProcessColor(processName){
        super.waitForElementToBeVisible('Waiting for infobox for item '+ processName + ' to be visible',
        processInfoBox, processName);
        return super.getElementCssAttribute('Getting the color of process with name ' + processName,
            processInfoBox, 'background-color', processName);
    }

    isInfoBoxIconExclamation(processName){
        return super.isElementDisplayed("Checking if icon for process '" + processName + "' is 'Exclamation'",
            processExclamationIcon, processName);
    }
};
