import winston from 'winston';
import fs from 'fs';


const env = process.env.NODE_ENV || 'development';
const logDir = 'log';
// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir);
}

let tsFormat = () => (new Date()).toLocaleDateString()
    + ": " + (new Date()).toLocaleTimeString();

export default class WinstonLogger{
    constructor(){
        this.winstonLogger = new (winston.Logger)({
            transports: [
                new (winston.transports.Console)({
                    timestamp: tsFormat,
                    colorize: true
                }),
                new (winston.transports.File)({
                    filename: `${logDir}/results.log`,
                    timestamp: tsFormat,
                    level: env === 'development' ? 'debug' : 'info'
                  })
            ]
        });
        this.winstonLogger.level = 'silly';
        this.winstonLogger.cli();
    }

    logInfo(logMessage){
        this.winstonLogger.info(logMessage);
    }

    logDebug(logMessage){
        this.winstonLogger.debug(logMessage);
    }
}
