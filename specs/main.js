import assert from 'assert';
import webdriver from 'selenium-webdriver';
import test from 'selenium-webdriver/testing';
import settings from '../config/settings.json';
import WebActions from '../lib/actions/web-actions.js';
import Css from '../lib/locators/by-css.js';
import data from '../config/test-case-data.json';

require('../node_modules/chromedriver');

let driver;
const mochaTimeOutMS = settings.mochaTimeOut;
const elementTimeOut = settings.elementTimeOut;


test.before(function () {
  this.timeout(mochaTimeOutMS);
  driver = new webdriver.Builder().forBrowser("chrome").build();
  driver.manage().timeouts().implicitlyWait(elementTimeOut);
  driver.manage().window().maximize();
  this.webActions = new WebActions(driver);
});

test.describe('Test-case for login action', function () {
  this.timeout(mochaTimeOutMS);

  test.it('will login', function () {
    driver.get(settings.loginUrl);
    this.webActions.loginActions.waitForPageToLoad();
    driver.getCurrentUrl().then(url => {
      assert.equal(url, settings.loginUrl, 'Current url does not match requested login url.');
    });
    this.webActions.loginActions.login(settings.credentials.login, settings.credentials.password);
  });
});

test.describe("Test-case 'Create Process'", function () {
  this.timeout(mochaTimeOutMS);

  test.it('will open processes', function () {
    this.webActions.mainActions.waitForPageToLoad();
    this.webActions.mainActions.openProcesses();
    this.webActions.processesActions.waitForPageToLoad();    
  });

  test.it('will create process with name ' + data.processName, function () {
    this.webActions.processesActions.createNewProcess(data.processName);
    this.webActions.processActions.waitForPageToLoad();
    this.webActions.processActions.getProcessName().then(text => {
      assert.equal(
        text,
        data.processName,
        'Created process name is incorrect. Expected `' + data.processName + '`. Found `' + text + '`'
      )
    });
  });

  test.it('will edit created process', function () {
    this.webActions.mainActions.clickProcessesItem();
    this.webActions.mainActions.waitForPageToLoad();
    driver.getCurrentUrl().then(url => {
      assert.equal(
        url, settings.processesListUrl, 'Processes list web page should be opened. Expected `' +
        settings.processesListUrl + '`. Found `' + url + '`'
      );
    });
    var previousColor = this.webActions.processesActions.getProcessColor(data.processName);
    this.webActions.processesActions.clickProcessEditIcon(data.processName);
    this.webActions.processesActions.clickColorPicker();
    this.webActions.processesActions.pickColorHue(1);
    this.webActions.processesActions.pickColor(99,0);
    this.webActions.processesActions.changeProcessIcon();
    this.webActions.processesActions.clickEditPopUpDoneButton();


    this.webActions.processesActions.getProcessColor(data.processName).then( currentColor =>{
      assert.notEqual(
        currentColor, previousColor,
        "Color of the process wasn't changed!"
      );
    });
    this.webActions.processesActions.checkForIconChanges(data.processName).then( changed =>{
      assert.equal( 
        changed, true,
        "Icon for process wasn't changed to 'Exclamation'");
    });
  });
});

test.describe("Test-case 'Create Workflow'", function () {
  this.timeout(mochaTimeOutMS);

  test.it('will open process ' + data.processName, function(){
    driver.sleep(5000); //replace this with some wait for element stainless for modal forms
    this.webActions.processesActions.openProcess(data.processName);
    this.webActions.processActions.waitForPageToLoad();
    this.webActions.processActions.isProcessPageLoaded(data.processName).then( opened => {
      assert.equal(
        opened, true,
        'Process page for process ' + data.processName + " wasn't opened."
      )
    });
  });
  test.it('will create workflow', function () {
    this.webActions.processActions.waitForPageToLoad();
    this.webActions.processActions.isWorkflowCanvasDisplayed().then(displayed => {
      assert.equal(displayed, true, "Workflow canvas isn't displayed");
    });
    this.webActions.processActions.createWorkflow(data.workflow.workflowName, data.workflow.objectName);
    this.webActions.processActions.isWorkflowCreated().then(created => {
      assert.equal(created, true, "New workflow wasn't created");
    });
  });
});

test.describe("Test-case 'Edit Workflow'", function () {
  this.timeout(mochaTimeOutMS);

  test.it('will edit workflow name and object name in modal form', function () {
    this.webActions.processActions.editWorkflowInModal(
      data.workflow.workflowName1stEdit, data.workflow.objectName1stEdit);
    this.webActions.processActions.checkForEditsToApply().then(text => {
      assert.equal(text,
        data.workflow.workflowName1stEdit,
        "Workflow name after edits should be: " + data.workflow.workflowName1stEdit +
        ". Found: " + text);
    })
  });
  test.it('will open admin screen for created workflow', function () {
    this.webActions.processActions.openWorkflowAdminScreen();
    this.webActions.processActions.checkAdminScreenForWorkflow().then(name => {
      assert.equal(name, data.workflow.workflowName1stEdit, "Admin screen for"
        + data.workflow.workflowName1stEdit + " wasn't opened");
    });
  })

  test.it('will edit workflow name and object name in admin screen', function () {
    this.webActions.processActions.editWorkflowInAdminScreen(
      data.workflow.workflowName2ndEdit, data.workflow.objectName2ndEdit);
    this.webActions.processActions.checkForEditsToApply().then(text => {
      assert.equal(text, data.workflow.workflowName2ndEdit, "Workflow name after edits should be "
        + data.workflow.workflowName2ndEdit);
    })
  });
});

test.describe('Test for creating and editing new node', function () {
  this.timeout(mochaTimeOutMS);

  test.it('will create and name a node', function () {
    this.webActions.processActions.waitForPageToLoad();
    this.webActions.processActions.isWorkflowCanvasDisplayed().then(displayed => {
      assert.equal(displayed, true, "Workflow canvas isn't displayed");
    });
    this.webActions.processActions.createNode(data.node.newNodeName);
    this.webActions.processActions.isNodeWithNameExists(data.node.newNodeName).then(displayed => {
      assert.equal(displayed, true, "Either new node with name " + data.node.newNodeName + " wasn't created or created with different name.");
    });
  });

  test.it('will create and name a node', function () {
    this.webActions.processActions.openNodeEditForm(data.node.newNodeName);
    this.webActions.processActions.renameNodeViaModalForm(data.node.newNodeName1stEdit);
    this.webActions.processActions.isNodeWithNameExists(data.node.newNodeName1stEdit).then(displayed => {
      assert.equal(displayed, true, "Node name wasn't changed to " + data.node.newNodeName1stEdit);
    });
  });
});

test.describe("Snapshot for test-case 'Edit Node'", function () {
  this.timeout(mochaTimeOutMS);

  test.it("will rename 'Default Origin' node", function () {
    this.webActions.processActions.waitForPageToLoad();
    this.webActions.processActions.isWorkflowCanvasDisplayed().then(displayed => {
      assert.equal(displayed, true, "Workflow canvas isn't displayed");
    });
    this.webActions.processActions.renameNodeNameViaShiftClick(data.node.originNodeName, data.node.originNodeNewName);
    this.webActions.processActions.isNodeWithNameExists(data.node.originNodeNewName).then(displayed => {
      assert.equal(displayed, true, "Node name wasn't changed to " + data.node.originNodeNewName);
    });
  });

  test.it("will rename 'Default End' node", function () {
    this.webActions.processActions.renameNodeNameViaShiftClick(data.node.endNodeName, data.node.endNodeNewName);
    this.webActions.processActions.isNodeWithNameExists(data.node.endNodeNewName).then(displayed => {
      assert.equal(displayed, true, "Node name wasn't changed to " + data.node.endNodeNewName);
    });
  });

  test.it('will move node', function () {
    this.webActions.processActions.waitForPageToLoad();
    this.webActions.processActions.isWorkflowCanvasDisplayed().then(displayed => {
      assert.equal(displayed, true, "Workflow canvas isn't displayed");
    });
    this.webActions.processActions.dragAndDropNode(data.node.newNodeName1stEdit, 100, 200);
  });
  test.it('will create transition between two nodes', function () {
    this.webActions.processActions.createTransitionBetweenNodes(data.node.originNodeNewName, data.node.newNodeName1stEdit);
  });
  test.it('will change node role and origin property', function () {
    this.webActions.processActions.openNodeEditForm(data.node.originNodeNewName);
    this.webActions.processActions.editNodeRoleAndOriginStatus();
  });
  //TODO: split to different tests and manage asserts
  test.it('will change node role and origin property', function () {
    this.webActions.mainActions.openAccess();
    this.webActions.accessActions.openRoleModalForm();
    this.webActions.accessActions.addAllNodesInProcessToRole(data.processName);

    this.webActions.mainActions.openDashboard();

    driver.sleep(5000);
  });
});

test.afterEach(function () {

});

test.after(function () {
 driver.manage().deleteAllCookies();
  driver.quit();
});